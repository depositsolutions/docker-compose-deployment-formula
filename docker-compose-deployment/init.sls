git package is installed:
  pkg.installed:
    - name: git

{%- for application, properties in salt['pillar.get']('docker-compose-deployment').items() %}

{{ application }} directory is present:
  file.directory:
    - name: {{ properties.directory }}

{{ application }} git repository is cloned:
  git.latest:
    - name: {{ properties.repository }}
    - target: {{ properties.directory }}/code
    - branch: {{ properties.branch }}
    - require:
      - pkg: git

{{ application }} stop current deployment:
  module.run:
    - name: dockercompose.kill
    - cwd: {{ properties.directory }}/code
    - path: '{{ properties.directory}}/code/{{ properties.compose_location }}'

{{ application }} remove current deployment:
  module.run:
    - name: dockercompose.rm
    - cwd: {{ properties.directory }}/code
    - path: '{{ properties.directory}}/code/{{ properties.compose_location }}'
    - require:
      - module: {{ application }} stop current deployment

{{ application }} deploy with compose:
  module.run:
    - name: dockercompose.up
    - cwd: {{ properties.directory }}/code
    - path: '{{ properties.directory}}/code/{{ properties.compose_location }}'
    - require:
      - module: {{ application }} remove current deployment
{%- endfor %}
